# Générateur calendrier scolaire

Générateur de calendriers scolaires et de différents types de plannings (périodes, semaines...), à partir d'un import ICAL des vacances scolaires (https://www.data.gouv.fr/fr/datasets/le-calendrier-scolaire/). 

Il est encore très perfectible mais fonctionnel.

Cet outil a été créé avec l'outil Microsoft Excel 2021.
Celui-ci ne fonctionne malheureusement que sur Excel et je n'ai pas les compétences pour en faire une version LibreOffice.
Si quelqu'un est intéressé pour faire cette conversion, c'est avec plaisir !

Thierry Munoz propose un générateur équivalent sous LibreOffice et le met en ligne ici.

Remerciements à ARCHAMBAULT Fanny, Thierry Munoz et Arnaud Champollion !

Ces documents sont placés sous licence GNU/GPL
